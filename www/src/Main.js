Santa.Main = {

    init: function(message) {
        this.message = message;
    },

    create: function() {

        this.background = new Santa.Background(this.game, 0.2);
        this.background.scroll();

        this.santa = new Santa.Santa(this.game, this.game.world.width /2, this.game.world.centerY);

        this.game.input.onDown.add(this.startGame, this);

        var title = this.game.add.text(0, -200, 'Santa Swoop', {font:'80px Lobster', fill:'#F22', boundsAlignH: "center", boundsAlignV: "middle"});
        title.setShadow(3, 3, 'rgba(0,0,0,0.5)', 2);
        title.setTextBounds(0, 0, this.game.world.width, 0);
        this.game.add.tween(title).to( { y: this.game.world.centerY-200}, 1000, Phaser.Easing.Bounce.In, true);

        var text = this.game.add.text(0, this.game.world.height /2 + 150, 'Touch to start! ', {font:'36px Lobster', fill:'#F22', boundsAlignH: "center", boundsAlignV: "middle"});
        text.setShadow(3, 3, 'rgba(0,0,0,0.5)', 2);
        text.setTextBounds(0, 0, this.game.world.width, 0);


        this.hohoho = this.game.add.audio('hohoho');
        this.game.time.events.add(500, function() {

        }, this);

        if (CONFIG.debug) {
            this.fpsText = this.game.add.text(0, 0, '--', {font:'32px Arial', fill:'#F22'});
            this.game.time.events.loop(1000, this.updateFps, this);
        }

        if(window.AdMob) window.AdMob.showBanner();
    },
    startGame:  function() {

        if(window.AdMob) window.AdMob.hideBanner();
        this.hohoho.play();

        if (this.game.time.fps > 45) {
            CONFIG.fps = true;
        }

        this.state.start("Game");
    },
    updateFps : function() {
        this.fpsText.text = this.game.time.fps + ' ';
    }

};