CONFIG = {
    fps : 0,
    debug : false,
    gameCnt: 0
};


(function () {

    var WIDTH = 1405;
    var HEIGHT = 768;

    var ratio = window.innerWidth/window.innerHeight;

    var game = new Phaser.Game( HEIGHT*ratio, HEIGHT, Phaser.CANVAS, 'game');

    game.state.add('Boot', Santa.Boot);
    game.state.add('Preload', Santa.Preload);
    game.state.add('Main', Santa.Main);
    game.state.add('Game', Santa.Game);

    game.state.start('Boot');

})();