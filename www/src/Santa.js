Santa.Santa = function(game, x, y) {
    Phaser.Sprite.call(this, game, 0, 0, 'santa', 1);

    this.x = x;
    this.y = y;
    this.game.physics.arcade.enable(this);
    this.body.collideWorldBounds = true;

    this.body.setSize(255, 95, 5, 5);

    this.anchor.setTo(0.5);
    this.scale.setTo(1);

    this.animations.add('fly', [1,2,3,4,5,6,7,8,9,10], 15, true);
    this.play('fly');

    game.add.existing(this);

    this.whoosh = this.game.add.audio('whoosh');
};

Santa.Santa.prototype = Object.create(Phaser.Sprite.prototype);
Santa.Santa.prototype.constructor = Santa.Santa;

Santa.Santa.prototype.fly = function() {
    this.body.velocity.y = -400;
    this.whoosh.play();
};