Santa.Background = function(game, speed) {
  this.game = game;

    if (speed) {
        this.speed = speed;
    } else {
        this.speed = 1;
    }

    this.bg = this.game.add.sprite(0, 0, 'bg-1');
    this.bg.width = this.game.world.width;

    if (this.game.time.fps > 55) {
        this.mountain = this.game.add.tileSprite(0, 0, this.game.world.width, this.game.world.height, 'bg-layer-4');
    }

    this.ground = this.game.add.tileSprite(0, 0, this.game.world.width, this.game.world.height, 'bg-layer-5');

};


Santa.Background.prototype = {

    scroll : function() {
        if (this.mountain) {
            this.mountain.autoScroll(-50*this.speed, 0);
        }

        this.ground.autoScroll(-250*this.speed, 0);
    },

    pause : function() {
        if (this.mountain) {
            this.mountain.autoScroll(0, 0);
        }

        this.ground.autoScroll(0, 0);
    },

    setSpeed : function(speed) {
        this.speed = speed;
        this.scroll();
    }

};