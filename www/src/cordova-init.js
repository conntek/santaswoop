/**
 * Initialize Cordova plugins
 * For more Corova plugins, please go to [Cordova Plugin Registry](http://plugins.cordova.io/#/).
 * In Intel XDK, you can enable / disable / add Cordova Plugins on
 * Projects Tab
 *  -> Cordova 3.x Hybrid Mobile App Settings
 *     -> Plugins and Permissions
 */
/* jshint browser:true */
// Listen to deviceready event which is fired when Cordova plugins are ready
document.addEventListener('deviceready', function() {

    var str;

    if (window.Cordova) {
        str = "It worked! Cordova device ready";
    } else if (window.intel && intel.xdk) {
        str = "It worked! Intel XDK device ready detected";
    } else {
        str = "Bad device ready, or none available because we're running in a browser.";
    }

    console.log(str);


});