Santa.Level = function(game) {

    this.game = game;
    this.data = JSON.parse(this.game.cache.getText('level'));

    this.obstacles = this.game.add.group();
    this.obstacles.enableBody = true;

    this.gifts = this.game.add.group();
    this.gifts.enableBody = true;

    this.game.time.events.loop(3500, this.loadNext, this);

    this.levelCnt = -1;

    this.speed = 1;
};

Santa.Level.prototype = {
    loadNext : function() {

        var next = Math.floor((Math.random() * this.data.sections.length));

        if  (this.levelCnt == next) {
            next++;
        }

        this.levelCnt = next;

        if (this.levelCnt >= this.data.sections.length) {
            this.levelCnt = 0;
        }

        var section = this.data.sections[this.levelCnt];

        this.currSpeed = this.speed;

        section.gifts.forEach(this.addGift, this);

        section.obstacles.forEach(this.addObstacle, this);

    },

    setSpeed: function (speed) {
        this.speed = speed;
    },

    addGift : function(data) {
        var gift = this.gifts.getFirstDead();

        if (!gift) {
            gift = this.gifts.create(0, 0, data.key);
            gift.scale.setTo(0.5);
            gift.body.collideWorldBounds = false;
            gift.body.allowGravity = false;
            gift.body.setSize(gift.width, gift.height, 0, 0);
        }

        gift.reset(this.game.world.width+data.x, data.y);
        gift.loadTexture(data.key);
        gift.body.velocity.x = -400 * this.currSpeed;
    },

    addObstacle : function(data) {
        var obstacle = this.obstacles.getFirstDead();

        if (!obstacle) {
            obstacle = this.obstacles.create(0, 0, data.key);
            obstacle.scale.setTo(0.5);
            obstacle.body.collideWorldBounds = false;
            obstacle.body.allowGravity = false;
            obstacle.body.setSize(obstacle.width, obstacle.height, 0, 0);
        }

        obstacle.reset(this.game.world.width+data.x, data.y);
        obstacle.loadTexture(data.key);
        obstacle.body.velocity.x = -400 * this.currSpeed;
    }
};