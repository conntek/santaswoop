// create Game function in BasicGame
Santa.Game = function (game) {
};

// set Game function prototype
Santa.Game.prototype = {

    init: function () {
        this.game.physics.startSystem(Phaser.Physics.ARCADE);
        this.game.physics.arcade.gravity.y = 1000;

        CONFIG.gameCnt++;

    },

    create: function () {

        this.speed = 1.0;

        this.background = new Santa.Background(this.game);
        this.background.scroll();

        this.santa = new Santa.Santa(this.game, this.game.world.width /2, this.game.world.height / 2);

        this.game.add.tween(this.santa).to( { x: this.game.world.centerX-this.game.world.width*0.2}, 2000, Phaser.Easing.Back.Out, true);

        this.game.input.onDown.add(this.santa.fly, this.santa);

        this.level = new Santa.Level(this.game);

        this.score = 0;
        this.speedInc = 20;
        this.nextSpeedUp = this.speedInc;
        this.scoreGift = this.add.sprite(this.game.world.width-200, 50, 'gift-score');
        this.scoreGift.scale.setTo(0.25);
        this.scoreText = this.game.add.text(this.game.world.width-130, 50,  this.score+ ' ', {font:'46px Lobster', fill:'#F22'});
        this.scoreText.setShadow(3, 3, 'rgba(0,0,0,0.5)', 2);


        if (this.game.time.fps > 45) {
            this.emitter = this.game.add.emitter(this.game.world.centerX+300, 0, 50);
            this.emitter.width = this.game.world.width;
            this.emitter.makeParticles('flake1');
            this.emitter.minParticleScale = 0.01;
            this.emitter.maxParticleScale = 0.15;

            this.emitter.setYSpeed(100, 300);
            this.emitter.setXSpeed(-500, -300);

            this.emitter.minRotation = 0;
            this.emitter.maxRotation = 0;

            this.emitter.start(false, 1000, 50, 0);
        }

        this.bellSound = this.game.add.audio('bell');

        this.crashSound = this.game.add.audio('crash');


        if(this.cache.isSoundDecoded('music')) {
            this.music = this.game.add.audio('music');
            this.music.loop = true;
            this.music.play();
        }


        var x = this;
        document.addEventListener("pause", function() {
            if (x.music) {
                x.music.pause();
            }

        }, false);
        document.addEventListener("resume", function() {
            if (x.music) {
                x.music.play();
            }
        }, false);

    },

    update: function() {

        if (this.game.physics.arcade.isPaused == false) {
            this.game.physics.arcade.overlap(this.santa, this.level.obstacles, this.crash, null, this);
            this.game.physics.arcade.overlap(this.santa, this.level.gifts, this.collectGift, null, this);
        }

        if (this.score >= this.nextSpeedUp) {
             this.speedUp();
        }

    },

    render: function() {
        if (CONFIG.debug) {
            this.game.debug.body(this.santa);
            this.level.obstacles.forEachAlive(function(o) {this.game.debug.body(o);}, this);
        }

    },

    crash: function() {
        this.game.physics.arcade.isPaused = true;
        this.crashSound.play();

        this.santa.animations.stop();
        this.santa.frame = 0;

        var text = this.game.add.text(0, this.game.world.centerY, 'Game Over! ', {font:'100px Lobster', fill:'#F22', boundsAlignH: "center", boundsAlignV: "middle"});
        text.setShadow(3, 3, 'rgba(0,0,0,0.5)', 2);
        text.setTextBounds(0, 0, this.game.world.width, 0);

        this.background.pause();

        if (this.music) {
            this.music.loop = false;
            this.music.pause();
        }

        if (this.emitter) {
            this.emitter.on = false;
        }


        if (CONFIG.gameCnt == 2) {
            if(window.AdMob) window.AdMob.showInterstitial();
        }

        this.loadMain();

    },

    collectGift: function(santa, gift) {
        gift.destroy();
        this.bellSound.play();
        this.score++;
        this.scoreText.text = this.score + ' ';
    },

    loadMain : function() {
        this.game.time.events.add(2000, function() {
            this.game.state.start('Main');
        }, this);

    },
    speedUp : function() {
        this.speed += 0.1;

        this.nextSpeedUp += this.speedInc;
        this.background.setSpeed(this.speed);
        this.level.setSpeed(this.speed);
    }

};