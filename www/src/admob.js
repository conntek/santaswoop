
var admobid = {};
if( /(android)/i.test(navigator.userAgent) ) {
    admobid = { // for Android
        banner: 'ca-app-pub-3377304321585177/3466358847',
        interstitial: 'ca-app-pub-3377304321585177/4943092046'
    };
} else if(/(ipod|iphone|ipad)/i.test(navigator.userAgent)) {
    admobid = { // for iOS
        banner: 'ca-app-pub-6869992474017983/4806197152',
        interstitial: 'ca-app-pub-6869992474017983/7563979554'
    };
} else {
    admobid = { // for Windows Phone
        banner: 'ca-app-pub-6869992474017983/8878394753',
        interstitial: 'ca-app-pub-6869992474017983/1355127956'
    };
}

if(( /(ipad|iphone|ipod|android|windows phone)/i.test(navigator.userAgent) )) {
    document.addEventListener('deviceready', initApp, false);
} else {
    initApp();
}

function initApp() {

    if (! window.AdMob ) {     document.getElementById("debugText").innerHTML = 'admob plugin not ready'; return; }

    window.AdMob.createBanner( {
        adId: admobid.banner,
        isTesting: false,
        overlap: true,
        offsetTopBar: true,
        position: AdMob.AD_POSITION.TOP_CENTER,
        bgColor: 'black',
        autoShow: false
    } );

    window.AdMob.prepareInterstitial({
        adId: admobid.interstitial,
        isTesting: false,
        autoShow: false
    });
}

