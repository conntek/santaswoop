Santa.Preload =  {

    preload: function () {

        this.game.time.advancedTiming = true;


        this.promo = this.add.sprite(0, 0, 'promo');
        this.promo.width = this.game.world.width;
        this.promo.height = this.game.world.height;

        this.bar = this.add.sprite(this.game.world.centerX, this.game.world.centerY - 100, 'bar');
        this.bar.anchor.set(0.5);
        this.bar.scale.set(3);
        this.bar.width = 600;
        this.bar.x = this.game.world.width/2;
        this.bar.y = this.game.world.centerY - 100;

        this.load.setPreloadSprite(this.bar);

        this.load.image('bg-1', 'asset/layers/bg-1.png');
        this.load.image('bg-layer-4', 'asset/layers/bg-layer-4.png');
        this.load.image('bg-layer-5', 'asset/layers/bg-layer-5.png');

        this.load.image('block1', 'asset/elements/block1.png');
        this.load.image('block2', 'asset/elements/block2.png');
        this.load.image('block3', 'asset/elements/block3.png');
        this.load.image('block4', 'asset/elements/block4.png');

        this.load.image('gift1', 'asset/elements/gift1.png');
        this.load.image('gift2', 'asset/elements/gift2.png');
        this.load.image('gift3', 'asset/elements/gift3.png');
        this.load.image('gift-score', 'asset/elements/gift-score.png');

        this.load.spritesheet('santa', 'asset/santa/santa-sheet.png', 305, 135, 11, 0, 1);

        this.load.image('flake1', 'asset/elements/flake1.png');

        this.load.audio('whoosh', 'asset/sound/whoosh.ogg', 'asset/sound/whoosh.mp3');
        this.load.audio('bell', 'asset/sound/bell.ogg', 'asset/sound/bell.mp3');
        this.load.audio('hohoho', 'asset/sound/hohoho.ogg', 'asset/sound/hohoho.mp3');
        this.load.audio('crash', 'asset/sound/crash.ogg', 'asset/sound/crash.mp3');
        this.load.audio('music', 'asset/sound/music.ogg', 'asset/sound/music.mp3');

        this.load.text('level', 'asset/data/level.json');
    },
    create: function() {
        this.timer = this.game.time.events.add(4000, function() {
            this.state.start("Main");
        }, this);
    },
    update: function() {
        if (this.cache.isSoundDecoded('music')) {
            this.state.start("Main");
        }
    }

};